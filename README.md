# Project title
###unlinkRAD

## Motivation

One of the assumptions behind analysing mutiple locus data in population genetics is that the loci are in linkage equilibrium (they are not linked). There are many software packages that can be used to assess whether linkage exists between loci and, for a small data set (tens of micosatellite loci), it is simple to remove linked loci. The linkage landscape becomes more complex when there are thousands or tens-of-thousands of loci with varying linkage relationships. The easiest way to deal with this scenario would be to delete all the loci that have been identified as linked. However, for some studies, that reduction may negate the analysis power gained by using many loci.

The aim of this project was to develop a method for selectively removing linked loci to retain the maximum number of loci.

## Methodology

The method that has been implemented in the unlinkRAD.pl script uses linkage information, output by programs such as the R package GENETICS (https://cran.r-project.org/web/packages/genetics/index.html), to iteratively remove loci from the datset starting with loci that have the most linkages, until no loci remain linked. The rationale behind this method is shown in the following simple three locus example:

![](unlink_graphic.png)

If we remove locus 2 (which has two linkages - shown in red) we immediately solve the linkage issue without losing locus 1 and 3 (which each have a single linkage). In situations where either of two loci can be removed to solve linkage, the script can either selectively remove a locus based on another quality measure (eg locus call-rate, coverage) or randomly. 


## Usage

Help on the script syntax can be obtained by running:

```sh
perl unlinkRAD.pl --help
```

Briefly, The script takes two input files:  
	1. A tab delimited, upper triangular square matrix of linkage probabilities.  
	2. A tab delimited, data matrix with loci as rows and individuals as columns. 

Example input files can be found in the "examples" folder. These can be run with the software as follows:

```sh
perl unlinkRAD.pl --in SNPcalls.txt --lnprob LinkageProbabilities.txt
```

If you run this example more than once you will notice that the output may change. This behaviour is normal given that some ties are broken randomly.


*NOTE:* The script was orginally written to process data where the header line begins with "Locus". If the header line does not begin with "Locus", the script will need to be modified for use. The script also uses a measure of SNP quality ("call_rate" in the example) to select the best SNP when there is a choice of two loci to remove, however any dataset and measure of quality can be used as long as the marker names are in column 1 and the quality measure in column 2. If you do not have any suitable quality measure, then simply insert a column of "1"s and any ties will be broken randomly.

## Dependencies

Perl 5.8 or later

## Acknowledgements

Idea by Matthew DeBoo  
Development, implementation and coding by Terry Bertozzi

## Licence

This project is licensed under the terms of the MIT licence as detailed in LICENCE.txt.



