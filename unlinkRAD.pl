#!/usr/bin/perl

# This script is designed to take a file of genotypes for multiple loci and a matrix of "linkage" probabilities output from the
# R package GENETICS and systematically remove loci based on the number of other loci to which they are linked. Loci with better
# coverage are retained where possible to break ties, else ties are broken randomly.
#
#
#
# Terry Bertozzi
# 17 February, 2015.

use strict;
use warnings;
use File::Basename;
use Getopt::Long;

my $alpha = "0.05";
my %pairs;
my %marker_count;
my %rad;
my @rad_header;
my @errors;

my $infile='';  # Name of RAD input file
my $lnprob_file=''; # Name of linkage probablities file
my $help;

my $opt=GetOptions("in=s"=>\$infile,
                   "lnprob=s"=>\$lnprob_file,
                   'help'=>\$help);
 
   if (!($opt && $infile && $lnprob_file) || $help) {#check for the required inputs
        print STDERR "\nExample usage:\n\n";
        print STDERR "$0 [Options] --in <filename> --lnprob <filename>\n";
        print STDERR "Options:\n";
        print STDERR "      --help = This usage example\n\n";
        print STDERR "Required:\n";
        print STDERR "      --in <RAD file to filter (tab delimited)>\n"; # Locus, Coverage must be columns 1 and 2 respectively
        print STDERR "      --lnprob <linkage probabilities file (tab delimited)>\n";
        exit;
    }

my ($fname, $dir, $ext) = fileparse($infile,'\..*');
my $outfile=$fname."_unlinked".$ext;

#------ Get the RAD data ------
unless (open (IN, '<:crlf',$infile)) { #use PerlIO layer to handle windows files
    die ("Can't open input file $infile\n");
}

while (my $line =<IN>) { #keep the header lines for output
    if ($line=~/^(Locus)/i){
        push @rad_header, $line;
        next;
    }
    
    chomp $line;  
    my @cols = split(/\t/,$line,3);
       
    unless (exists($rad{$cols[0]})){
        $rad{$cols[0]}={call_rate=>$cols[1], genotype=>$cols[2]};
    } else{
         push @errors, $rad{$cols[0]};
    }
}

close IN;

# if errors, print message and quit
if (@errors) {
     print "Possible duplicate locus IDS: ";
     print join(",", @errors)."\n";
     exit;
}

# ------ Process the likage probability triangle ------
unless (open (LNPROB, '<:crlf', $lnprob_file)) { #use PerlIO layer to handle windows files
    die ("Can't open input file $lnprob_file\n");
}

# get the marker ids
my $header = <LNPROB>;
chomp $header;
$header=~s/^\t//; #strip the leading tab
my @header = split(/\t/,$header);
my @rev_header = reverse(@header);

# calculate the Bonferroni corrected alpha value
my $n = scalar(@header);
my $bonferroni = $alpha/(($n*($n-1))/2);

# process the pvalues
while (my $line =<LNPROB>) {
    chomp ($line);
    my @line = split(/\t/,$line);
    my $marker_id = shift @line;
    my @rev_line = reverse(@line);
    
    for(my $index =0; $index<= $n; $index++){
        last if $rev_line[$index] eq "NA";
        
        # if the pvalues are significant, keep the marker ids
        if ($rev_line[$index] < $bonferroni) {
            push @{$pairs{$marker_id}}, $rev_header[$index];
            
            # keep a count of each marker id    
            count(\%marker_count, $marker_id);
            count(\%marker_count, $rev_header[$index]);
        }   
    }
}

close LNPROB;

# ------ incrementally remove linked markers from the data ------
while (%pairs) {
    
    # find the most linked marker
    my $linked = largest_value (\%marker_count);
    
    # decrement the count of loci linked to it
    if (exists($pairs{$linked})) {
          foreach my $element(@{$pairs{$linked}}){
               $marker_count{$element}--;
               delete($marker_count{$element}) if ($marker_count{$element} == 0);
          }
    }
    
    # remove it
    delete($pairs{$linked});
    delete($marker_count{$linked});
    delete($rad{$linked});
    
    # unlink loci it was linked to
    foreach my $key (keys %pairs){
        my $element_count=scalar(@{$pairs{$key}});
        @{$pairs{$key}} = (grep{$_ ne $linked} @{$pairs{$key}}); #remove the linked marker from other pairs
        if (!@{$pairs{$key}}){ # it had only one value
            $marker_count{$key}--;
            delete($marker_count{$key}) if ($marker_count{$key} == 0);
            delete($pairs{$key});
        }elsif (scalar(@{$pairs{$key}}) < $element_count){
            $marker_count{$key}--;
            delete($marker_count{$key}) if ($marker_count{$key} == 0);
        }
    }
} 
    
# ----- Output all unlinked RAD data ------ 
unless (open (OUTFILE, ">", $outfile)) {
    die ("Can't open $outfile for output\n");
}

print OUTFILE @rad_header;
for my $marker (sort keys %rad){
    print OUTFILE "$marker\t$rad{$marker}{call_rate}\t$rad{$marker}{genotype}\n";
}

close OUTFILE;
exit;

# ------ Subroutines ------

sub break_tie {
    # Takes a reference to a hash and breaks the tie using the RAD coverage or randomly
    # if the coverage is equal.

    my $ref_hash = shift;
    my @small_keys;
    my $locus;
    my %ref_hash = %$ref_hash;
    
    keys %ref_hash;       # reset the each iterator
    
    my ($small_key, $small_val) = ("2","2"); #each %call_rate;
    
    # get the call rates for the markers with tied counts and determine the lowest value
    while (my ($key, $val) = each %ref_hash) {
        if (exists($rad{$key}{call_rate})) {
            if ($rad{$key}{call_rate} < $small_val) {
                $small_val = $rad{$key}{call_rate};
                @small_keys=();
                push @small_keys, $key; 
            }elsif ($rad{$key}{call_rate} == $small_val) {
                push @small_keys, $key;
            }
        } else{
            print "Call rate information missing for $key";
            exit;
        }
    }
    
    if (scalar @small_keys > "1") {       
        $locus = $small_keys[rand @small_keys]; #pick a marker at random
    }else{
        $locus= $small_keys[0];
    }
    
    return $locus;
}

sub count {
    # Takes a reference to a hash and key and increments the value. If the
    # key doesn't exist it is created
    
    my ($ref_marker_count, $key) = @_;
    my %ref_marker_count = %$ref_marker_count;

    if (exists($ref_marker_count{$key})){
        $$ref_marker_count{$key}++;
    }else{
        $$ref_marker_count{$key}="1";
    }
}


sub largest_value {
    # Takes a reference to a hash and returns the key with the largest value.
    # Ties are broken by a call to break_tie.
    
    my $hash = shift;
    my %large_key;
    my $locus;
    
    keys %$hash;       # reset the each iterator

    my ($large_key, $large_val) = ("0","0");

    while (my ($key, $val) = each (%$hash)) {
        if ($val > $large_val) {
            $large_val = $val;
            %large_key=();
            $large_key{$key}=$val; #store as hash to facilitate tie breaking
        }elsif ($val == $large_val) {
            $large_key{$key}=$val;
        }
    }

    # break ties
    if (scalar keys %large_key > "1") {       
        $locus = break_tie (\%large_key);
    }else{
        my @locus = keys %large_key; #need to call in list context
        $locus = $locus[0];
    }
    
    return $locus;
}
